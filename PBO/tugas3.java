package PBO;

public class tugas3 {
    public static void main(String[] args) {
        System.out.println("Menghitung Volume Tabung");
        System.out.println("==============================");
        System.out.println();

        double  r=2.5, phi=3.14;
        int t=10;

        System.out.println("Volume Tabung = "+String.format("%.2f", phi*r*t));
    }
}
